<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">
        <link rel="stylesheet" href="js/vendor/jquery.fancybox/jquery.fancybox.min.css">
        <link rel="stylesheet" href="css/main.css">

    </head>
    <body>

        <div class="page">

            <header class="header">
                <div class="container">
                    <div class="header__row">
                        <div class="header__logo">
                            <img src="img/logo.svg" class="img-fluid" alt="">
                        </div>
                        <div class="header__content">
                            <nav class="header__nav scroll_nav">
                                <ul>
                                    <li><a href="#service">О сервисе</a></li>
                                    <li><a href="#advantage">Преимущества</a></li>
                                    <li><a href="#partnres">Партнеры</a></li>
                                </ul>
                            </nav>
                            <ul class="header__contact">
                                <li>
                                    <span>Отдел продаж:</span>
                                    <a href="tel:+79997889633">+7 (999) 788 96 33</a>
                                </li>
                                <li>
                                    <span>Отдел продаж:</span>
                                    <a href="tel:+79997889633">+7 (999) 788 96 33</a>
                                </li>
                            </ul>

                            <div class="header__auth">
                                <a class="btn btn_white btn_sm" href="#">Войти</a>
                            </div>

                        </div>
                        <div class="header__content_layout nav_toggle"></div>
                        <a href="#" class="header__toggle nav_toggle">
                            <span></span>
                        </a>
                    </div>
                </div>
            </header>

            <section class="promo">
                <div class="container">
                    <div class="promo__row">
                        <div class="promo__content">
                            <h1><span class="txt-rotate" data-period="1000" data-rotate='[ "Бронирование билетов", "Управление поездками", "Командировочные отчеты"]'>Командировочные отчеты</h1>
                            <p>Все для командировок в одном месте!</p>
                            <div class="promo__button">
                                <a href="#" class="btn btn_shadow">Зарегистрироваться</a>
                            </div>
                        </div>
                        <div class="promo__image">
                            <img src="img/monitor.png" class="img-fluid" alt="">
                            <a href="https://youtu.be/q3YCB1_2bkY" class="promo__video" data-fancybox>
                                <span>
                                    <img src="img/icon__play.png" class="img-fluid" alt="">
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="promo__mobile">
                        <a href="#" class="btn btn_shadow">Зарегистрироваться</a>
                    </div>
                </div>
            </section>

            <section class="vendor hide">
                <div class="container">
                    <div class="vendor_slider swiper-container">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <div class="swiper-slide">
                                <div class="vendor__logo">
                                    <i><img src="img/logos/logo__01.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="vendor__logo">
                                    <i><img src="img/logos/logo__02.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="vendor__logo">
                                    <i><img src="img/logos/logo__03.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="vendor__logo">
                                    <i><img src="img/logos/logo__04.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="vendor__logo">
                                    <i><img src="img/logos/logo__05.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="vendor__logo">
                                    <i><img src="img/logos/logo__06.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="vendor__logo">
                                    <i><img src="img/logos/logo__07.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="about">
                <div class="container">
                    <h2>Бизнестур.Онлайн</h2>
                    <div class="about__introtext">Удобный инструмент для организации ваших командировок. Сократит расходы, автоматизирует процесс оформления — от выбора и бронирования билетов до получения закрывающих документов</div>
                    <ul class="about__row">
                        <li>
                            <div class="about__image">
                                <img src="img/about__image_01.png" class="img-fluid" alt="">
                            </div>
                            <div class="about__content">
                                <div class="about__title">Руководителю</div>
                                <div class="about__text">Поможем экономить на командировочных расходах и обеспечить их прозрачность</div>
                            </div>
                        </li>
                        <li>
                            <div class="about__image">
                                <img src="img/about__image_02.png" class="img-fluid" alt="">
                            </div>
                            <div class="about__content">
                                <div class="about__title">Бухгалтеру</div>
                                <div class="about__text">Облегчим труд и сэкономим время: оплату произведем с корпоративной карты, а закрывающие документы отправим в электронном виде моментально после покупки (ЭДО)</div>
                            </div>
                        </li>
                        <li>
                            <div class="about__image">
                                <img src="img/about__image_03.png" class="img-fluid" alt="">
                            </div>
                            <div class="about__content">
                                <div class="about__title">Сотруднику</div>
                                <div class="about__text">Обеспечим доступ ко всем travel-услугам в одном окне, а вопросы с закрывающими документами возьмем на себя</div>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>

            <section class="comfort" id="service">
                <div class="container">
                    <h2>Удобнее, чем покупка билетов напрямую</h2>
                    <div class="subtitle">Купить билет и забронировать отель по лучшей цене сэкономив при этом несколько часов, все это возможно с нашим сервисом</div>
                    <div class="comfort__row">

                        <div class="comfort__col">
                            <div class="comfort__title comfort__title_rose"><span>Покупка билетов через агентства</span></div>
                            <div class="comfort__informer comfort__informer_rose">Бумажная бюрократия, возня с оплатой и получением документов. Каждая командировка – стресс</div>
                            <div class="comfort__wrap comfort__wrap_rose">
                                <ul class="comfort__step comfort__step_rose">
                                    <li>Заключить договор<br/>с агентством</li>
                                    <li>Запросить счет и пополнить<br/>депозит</li>
                                    <li>Отправить заявку на <br/>бронирование/подобрать билеты <br/>самомостоятельно</li>
                                    <li>Согласовать подобранный <br/>вариант</li>
                                    <li>Оплатить бронирование <br/>билета</li>
                                    <li>Получить закрывающие <br/>документы</li>
                                    <li>Подписать и отправить оригиналы <br/>закрывающих документов</li>
                                </ul>
                            </div>
                        </div>

                        <div class="comfort__col">
                            <div class="comfort__title comfort__title_green"><span>Покупка билетов через Бизнестур.онлайн</span></div>
                            <div class="comfort__informer comfort__informer_green">5 минут и билеты с закрывающими документами у вас в личном кабинете!</div>
                            <div class="comfort__wrap comfort__wrap_green">
                                <ul class="comfort__step comfort__step_green">
                                    <li>
                                        <strong>Зарегистрироваться</strong>
                                        <span>2 минуты</span>
                                    </li>
                                    <li>
                                        <strong>Привязать корпоративную карту</strong>
                                        <span>Один раз</span>
                                    </li>
                                    <li>
                                        <strong>Оплатить билеты/отели</strong>
                                        <span>Средства спишутся с р/с компании</span>
                                    </li>
                                    <li>
                                        <strong>Получить закрывающие документы <br/>онлайн</strong>
                                        <span>ЭЦП</span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="comfort__man">
                            <img src="img/comfort__man.png" class="img-fluid" alt="">
                        </div>

                    </div>

                    <div class="text-center">
                        <a href="#" class="btn btn_shadow">Зарегистрироваться</a>
                    </div>

                </div>
            </section>

            <section class="advantage" id="advantage">
                <div class="container">
                    <h2 class="text-center">Наши преимущества</h2>
                    <div class="subtitle">Вы наглядно можете увидеть основные преимущества использования сервиса Бизнестур.онлайн</div>
                    <div class="advantage__mobile">
                        <div class="advantage_slider swiper-container">

                            <div class="swiper-wrapper">

                                <div class="swiper-slide">
                                    <div class="advantage__item advantage__item_01">
                                        <div class="advantage__item_icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 61.27 53.43" class="ico-svg">
                                                <defs>
                                                    <style>
                                                        .cls-1,
                                                        .cls-2,
                                                        .cls-3,
                                                        .cls-4,
                                                        .cls-5 { fill:none; stroke-linecap:round; stroke-linejoin:round; }
                                                        .cls-1{stroke-width:2.13px;}
                                                        .cls-2{stroke-width:2.13px;}
                                                        .cls-3{stroke-width:2.13px;}
                                                        .cls-4{stroke-width:2.13px;}
                                                        .cls-5{stroke-width:2.13px;}
                                                    </style>
                                                </defs>
                                                <title>Билеты и услуги в одном месте</title>
                                                <path class="cls-1" d="M280.07,371.06H331.2a4,4,0,0,1,4,4v34.42a4,4,0,0,1-4,4H280.07a4,4,0,0,1-4-4V375.07a4,4,0,0,1,4-4h0Z" transform="translate(-275 -370)"/>
                                                <path class="cls-2" d="M276.06,402.95H335.2m-40.14,19.42H316.2m-18.45,0,2-8.86m13.81,8.86-2-8.86m-5.92-7.36a1.64,1.64,0,1,1-1.64,1.64,1.64,1.64,0,0,1,1.64-1.64h0Z" transform="translate(-275 -370)"/>
                                                <path class="cls-3" d="M284.76,392.4H295.4V398H284.76V392.4h0Zm16.08,0h10.64V398H300.84V392.4h0Zm15.15,0h10.64V398H316V392.4h0Z" transform="translate(-275 -370)"/>
                                                <polygon class="cls-4" points="9.64 10.59 51.63 10.59 51.63 19.21 9.64 19.21 9.64 10.59 9.64 10.59"/>
                                                <line class="cls-3" x1="10.05" y1="6.9" x2="22.27" y2="6.9"/>
                                                <path class="cls-5" d="M324.52,376.9h2.1m-7.13,0h2.1m-7.13,0h2.1" transform="translate(-275 -370)"/>
                                            </svg>
                                        </div>
                                        <div class="advantage__item_title">Билеты и услуги в одном месте</div>
                                        <div class="advantage__item_text">Не нужно открывать разные вкладки, сравнивать цены и бронировать услуги на разных сайтах.</div>
                                    </div>
                                    <div class="advantage__item advantage__item_02">
                                        <div class="advantage__item_icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 49.8 53.15" class="ico-svg">
                                                <defs>
                                                    <style>
                                                        .cls-1{fill:none; stroke-linecap:round;stroke-linejoin:round;stroke-width:2.13px;}
                                                    </style>
                                                </defs><title>Все данные под рукой</title>
                                                <path class="cls-1" d="M287.63,402.11h12.19v11H287.63v-11h0Zm23.2,14.44h17.91v5.54H310.83v-5.54h0Zm16.34,0v-3.22l1.24-3.68v-4.93c0-2.32-1.4-3.69-3.93-4.28l-6.77-1.19v-7.46c0-4-4.73-4.23-4.73,0v11.59l-3.09-2.29c-2.24-1.34-4.88.35-2.39,3.58,1.91,3,2.87,6.83,5.72,8.91,0,1-1,3,0,3h13.93Zm-35.79-27.22,3.18,3,6.37-7.2m-0.76,6.68v3.7H287.81v-11h7.87m27,0.88H305.88m3.62,4.66h-3.62m3.62,4.44h-3.62m-21.28-20h1.78m2.4,0h1.78m16.64,45.39H281.06V371.06h46.38v25.46m0-18.64H281.06" transform="translate(-280 -370)"/>
                                            </svg>
                                        </div>
                                        <div class="advantage__item_title">Все данные под рукой</div>
                                        <div class="advantage__item_text">Документы и данные обо всех командировках надежно хранятся в личном кабинете.</div>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="advantage__item advantage__item_03">
                                        <div class="advantage__item_icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 59.12 53.29" class="ico-svg">
                                                <defs>
                                                    <style>
                                                        .cls-1{fill:none; stroke-linecap:round;stroke-linejoin:round;stroke-width:2.13px;}
                                                    </style>
                                                </defs>
                                                <title>Экономия времени</title>
                                                <path class="cls-1" d="M321.86,397.84a12.2,12.2,0,1,1-12.2,12.2,12.2,12.2,0,0,1,12.2-12.2h0Zm7.33,2.46,1.84-2.44m-16.87,2.44-1.9-2.46m9.54,4.84V411m-3.63-18.69h7.27m-3.63,5.48v-5.48M332,393.61V371.06H277.06V417.2h29.1m-4.71-11.11H281.55m11.2,5h-11.2m0-27.43h19.11v16.48H281.55V383.7h0Zm-4.48-4.63H332m-50.45-3.84h1m2.64,0h1" transform="translate(-276 -370)"/>
                                            </svg>
                                        </div>
                                        <div class="advantage__item_title">Экономия времени</div>
                                        <div class="advantage__item_text">Бронирование билетов и отелей занимает не более 5 минут. После поездки поможем составить командировочный отчет.</div>
                                    </div>
                                    <div class="advantage__item advantage__item_04">
                                        <div class="advantage__item_icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.21 54.66" class="ico-svg">
                                                <defs>
                                                    <style>
                                                        .cls-1{fill:none; stroke-miterlimit:22.93;stroke-width:2.13px;}
                                                    </style>
                                                </defs>
                                                <title>Отчетность</title>
                                                <path class="cls-1" d="M309.94,413.46h-27.5a2.49,2.49,0,0,1-2.38-2.38V372.76c0-1.65.63-2.7,2.7-2.7h45.57a2.53,2.53,0,0,1,2.81,2.81V411c0,1.63-.69,2.3-1.56,2.3h-4.51M284.7,381.7h16.72v11.53H284.7V381.7h0ZM284,399h17.44M284,404h17.44M284,408.7h12.13M305.8,383h20.59M305.8,387.6h14.32M282.55,377h46.1m-44.77-3.41h1.92m1.34,0h1.92m31.85,38.11v10l-3.33-1.92-3.67,2.12V411.54l0.59,0.69,3-.76,3,0.71,0.44-.53h0Zm-0.59-18.14,2,2.34,2.86,1.14,0.25,3.07,1.64,2.6-1.6,2.63-0.2,3.07-2.84,1.18-2,2.37-3-.71-3,.76-2-2.34-2.86-1.14-0.25-3.07-1.64-2.6,1.6-2.63,0.2-3.07,2.84-1.18,2-2.37,3,0.71,3-.76h0Zm-2.91,5.63a3.59,3.59,0,1,1-3.59,3.59,3.59,3.59,0,0,1,3.59-3.59h0Z" transform="translate(-279 -369)"/>
                                            </svg>
                                        </div>
                                        <div class="advantage__item_title">Отчетность</div>
                                        <div class="advantage__item_text">Закрывающие документы в электронном виде. Подпишем их с ЭЦП и отправим в 1С или личный кабинет оператора ЭДО.</div>
                                    </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="advantage__item advantage__item_05">
                                            <div class="advantage__item_icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.04 53.05" class="ico-svg">
                                                    <defs>
                                                        <style>
                                                            .cls-1{fill:none; stroke-linecap:round;stroke-linejoin:round;stroke-width:2.13px;}
                                                        </style>
                                                    </defs>
                                                    <title>Понятные и прозрачные цены</title>
                                                    <path class="cls-1" d="M331,371.07H284.61c-5.42-.15-6.06,7.54,0,8H330.7v43H280.06V376.09c0-3.11,1.52-5,4.55-5m0,8c-2.28,0-3.68-1.45-4.51-3.83M310,391.45H330.7v18.21H310a1.24,1.24,0,0,1-1.23-1.23V392.68a1.24,1.24,0,0,1,1.23-1.23h0Zm6.5,5.74a3.37,3.37,0,1,1-3.37,3.37,3.37,3.37,0,0,1,3.37-3.37h0Zm-30.39-22.05H331" transform="translate(-279 -370)"/>
                                                </svg>
                                            </div>
                                            <div class="advantage__item_title">Понятные и прозрачные цены</div>
                                            <div class="advantage__item_text">Стоимость на билеты и отели без скрытых комиссий и сборов. С помощью нашей умной аналитики мы поможем правильно настроить travel-политику для еще большей экономии. </div>
                                        </div>
                                    <div class="advantage__item advantage__item_06">
                                            <div class="advantage__item_icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60.74 52.97" class="ico-svg">
                                                    <defs>
                                                        <style>
                                                            .cls-1{fill:none; stroke-linecap:round;stroke-linejoin:round;stroke-width:2.13px;}
                                                        </style>
                                                    </defs>
                                                    <title>Подробная статистика</title>
                                                    <path class="cls-1" d="M280,371.06h50.67a4,4,0,0,1,4,4v34.12a4,4,0,0,1-4,4H280a4,4,0,0,1-4-4V375a4,4,0,0,1,4-4h0Zm-4,31.6h58.62M294.9,421.91h20.94m-18.28,0,1.94-8.79m13.68,8.79-1.94-8.79m-5.87-7.29a1.63,1.63,0,1,1-1.63,1.63,1.63,1.63,0,0,1,1.63-1.63h0Zm-12.11-29.28a10.12,10.12,0,1,1-10.12,10.12,10.12,10.12,0,0,1,10.12-10.12h0Zm-1.67.14,1.67,10,7.61,6.66m-7.43-6.5,7.25-7m8-1.09h18.76m-18.76,5.39h18.76m-18.76,5.34h18.76m-18.76,5.44H320.1" transform="translate(-275 -370)"/>
                                                </svg>
                                            </div>
                                            <div class="advantage__item_title">Подробная статистика</div>
                                            <div class="advantage__item_text">Командировочные отчеты и расходные документы по каждому сотруднику, включающие дополнительные траты.</div>
                                        </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="advantage__item advantage__item_07">
                                            <div class="advantage__item_icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56.32 53.1" class="ico-svg">
                                                    <defs>
                                                        <style>
                                                            .cls-1,.cls-3{fill:none; stroke-width:2.13px;}
                                                            .cls-1{stroke-linecap:round;stroke-linejoin:round;}
                                                            .cls-3{stroke-miterlimit:22.93;}
                                                        </style>
                                                    </defs>
                                                    <title>Удобно оплачивать</title>
                                                    <path class="cls-1" d="M322,398.8a4.69,4.69,0,1,1-4.69,4.69A4.69,4.69,0,0,1,322,398.8h0Zm-31.5-5.21h40a1.8,1.8,0,0,1,1.79,1.79v24.86a1.8,1.8,0,0,1-1.79,1.79h-40a1.8,1.8,0,0,1-1.79-1.79V395.39a1.8,1.8,0,0,1,1.79-1.79h0Zm3.15,20.34h4.15m5.52,0h4.15m5.59,0h4.15m5.37,0h4.15" transform="translate(-277 -370)"/><path class="cls-2" d="M279.8,370h40a2.8,2.8,0,0,1,2,.82h0a2.79,2.79,0,0,1,.82,2v16.14h-2V372.8a0.77,0.77,0,0,0-.23-0.55h0a0.78,0.78,0,0,0-.55-0.23h-40a0.78,0.78,0,0,0-.55.23h0a0.77,0.77,0,0,0-.23.55v24.86a0.77,0.77,0,0,0,.23.55h0a0.77,0.77,0,0,0,.55.23H285v2h-5.2a2.8,2.8,0,0,1-2-.82h0a2.79,2.79,0,0,1-.82-2V372.8a2.8,2.8,0,0,1,.82-2h0a2.8,2.8,0,0,1,2-.82h0Z" transform="translate(-277 -370)"/>
                                                    <path class="cls-3" d="M278,378h43.55M278,384.63h43.55" transform="translate(-277 -370)"/>
                                                </svg>
                                            </div>
                                            <div class="advantage__item_title">Удобно оплачивать</div>
                                            <div class="advantage__item_text">Оплата билетов и отелей напрямую с расчетного счета. Не нужно пополнять депозиты и выдавать сотрудникам     наличные.</div>
                                        </div>
                                    <div class="advantage__item advantage__item_08">
                                            <div class="advantage__item_icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 61.76 54.16" class="ico-svg">
                                                    <defs>
                                                        <style>
                                                            .cls-1{}
                                                            .cls-2,.cls-3{fill:none; stroke-width:2.13px;}
                                                            .cls-2{stroke-miterlimit:22.93;}
                                                            .cls-3{stroke-linecap:round;stroke-linejoin:round;}
                                                        </style>
                                                    </defs>
                                                    <title>Один договор</title>
                                                    <path class="cls-1" d="M277.52,369h54.73a3.53,3.53,0,0,1,3.52,3.52v35a3.53,3.53,0,0,1-3.52,3.52h-5a2,2,0,0,0,.73-1.24,2.49,2.49,0,0,0,.11-0.89h4.12a1.39,1.39,0,0,0,1.39-1.39v-35a1.39,1.39,0,0,0-1.39-1.39H277.52a1.39,1.39,0,0,0-1.39,1.39v35a1.39,1.39,0,0,0,1.39,1.39h35a2.56,2.56,0,0,0,1.66,1.88,2.89,2.89,0,0,1,.3.24H277.52a3.53,3.53,0,0,1-3.52-3.52v-35a3.53,3.53,0,0,1,3.52-3.52h0Z" transform="translate(-274 -369)"/>
                                                    <path class="cls-2" d="M322.53,394.5l2,2.39L327.5,398l0.25,3.14,1.68,2.66-1.64,2.69-0.2,3.14-2.9,1.21-2,2.42-3.06-.73-3,.78-2-2.39-2.92-1.16-0.25-3.14L309.67,404l1.64-2.69,0.2-3.14,2.9-1.21,2-2.42,3.06,0.73,3-.78h0Zm-3,5.75a3.67,3.67,0,1,1-3.67,3.67,3.67,3.67,0,0,1,3.67-3.67h0Z" transform="translate(-274 -369)"/>
                                                    <path class="cls-3" d="M320.46,418.64l1.47,3.46,1.74-4.19,4,1.62-2.6-8.35m-5.2,1.83-3.09,8.87-2.08-4.46-3.94,1.83,1.71-5.66m-13.84-38.34h12.54m-19.57,3.39h26.6m-36.77,5.42h46.93m-46.93,4.74h46.93m-46.93,4.83h23m-12.48,6.23c3.62-1.54,5.51,1.62,4.64,3.94-0.75,2-2.55,2.18-4.31,1.4a3.1,3.1,0,0,1-1.87-2.89c0.21-2.94-2.92-2.51-2.78,0-0.51,1.09-1.11,1.4-1.9.05-0.54-1-1.08-1-1.62.23-0.91.87-1.44,0.78-1.67-.1a0.92,0.92,0,0,0-1.09-1.06" transform="translate(-274 -369)"/>
                                                </svg>
                                            </div>
                                            <div class="advantage__item_title">Один договор</div>
                                            <div class="advantage__item_text">Все закрывающие документы от единого поставщика.</div>
                                        </div>
                                </div>

                                <div class="swiper-slide">
                                    <div class="advantage__item advantage__item_09">
                                            <div class="advantage__item_icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66.88 47.75" class="ico-svg">
                                                    <defs>
                                                        <style>
                                                            .cls-1{}
                                                            .cls-2,.cls-3{fill:none; stroke-width:2.13px;}
                                                            .cls-2{stroke-miterlimit:22.93;}
                                                            .cls-3{stroke-linecap:round;stroke-linejoin:round;}
                                                        </style>
                                                    </defs>
                                                    <title>Специальные предложения</title>
                                                    <path class="cls-1" d="M275.34,374.77h22.23a2,2,0,0,0,.31,2.13H275.34a1.22,1.22,0,0,0-1.22,1.22V417.4a1.22,1.22,0,0,0,1.22,1.22h57.28a1.22,1.22,0,0,0,1.22-1.22v-4.46a2.84,2.84,0,0,0,2.13.69v3.77a3.36,3.36,0,0,1-3.34,3.34H275.34A3.36,3.36,0,0,1,272,417.4V378.12a3.36,3.36,0,0,1,3.34-3.34h0Zm32.75,0h24.52a3.36,3.36,0,0,1,3.34,3.34v24.45a2.82,2.82,0,0,0-2.13-.71V378.12a1.22,1.22,0,0,0-1.22-1.22H310.39a4.72,4.72,0,0,0-.74-1.06,4,4,0,0,0-1.26-.85l-0.29-.22h0Z" transform="translate(-272 -373)"/>
                                                    <path class="cls-1" d="M295.62,373h11.91a1.06,1.06,0,0,1,.81.38l7.64,7.74-1.08-.26-1,1.18-6.83-6.92h-8.93l11.28,11.42-0.11,1.68-0.46.76-14-14.18a1.06,1.06,0,0,1,.76-1.8h0Zm32.18,20.07L338.57,404a1.06,1.06,0,0,1,.3.81l-0.25,11.74a1.06,1.06,0,0,1-1.81.72l-16-16.2,0.92,0.22,1-1.25,13.8,14,0.19-8.83-9.56-9.68,0.1-1.57,0.51-.83h0Z" transform="translate(-272 -373)"/>
                                                    <polygon class="cls-2" points="49.56 7.8 51.79 10.4 54.97 11.67 55.24 15.09 57.07 17.98 55.29 20.91 55.07 24.32 51.91 25.64 49.72 28.27 46.39 27.48 43.07 28.33 40.84 25.73 37.66 24.46 37.38 21.05 35.55 18.15 37.34 15.23 37.56 11.81 40.72 10.49 42.91 7.86 46.24 8.65 49.56 7.8 49.56 7.8"/>
                                                    <path class="cls-3" d="M284.28,389.4a4.51,4.51,0,1,1-4.25,4.5,4.38,4.38,0,0,1,4.25-4.5h0Zm12.86,13a4.51,4.51,0,1,1-4.25,4.5,4.38,4.38,0,0,1,4.25-4.5h0Zm-15.48,8.93,17.87-22" transform="translate(-272 -373)"/>
                                                </svg>
                                            </div>
                                            <div class="advantage__item_title">Специальные предложения</div>
                                            <div class="advantage__item_text">У нас заключены прямые контракты с авиакомпаниями и отелями, поэтому мы предлагаем самые низкие цены.</div>
                                        </div>
                                    <div class="advantage__item advantage__item_0"></div>
                                </div>
                            </div>

                            <div class="swiper-pagination"></div>
                        </div>
                    </div>

                    <div class="advantage__desktop">
                        <div class="advantage__row">
                            <div class="advantage__col">
                                <div class="advantage__item advantage__item_01">
                                    <div class="advantage__item_icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 61.27 53.43" class="ico-svg">
                                            <defs>
                                                <style>
                                                    .cls-1,
                                                    .cls-2,
                                                    .cls-3,
                                                    .cls-4,
                                                    .cls-5 { fill:none; stroke-linecap:round; stroke-linejoin:round; }
                                                    .cls-1{stroke-width:2.13px;}
                                                    .cls-2{stroke-width:2.13px;}
                                                    .cls-3{stroke-width:2.13px;}
                                                    .cls-4{stroke-width:2.13px;}
                                                    .cls-5{stroke-width:2.13px;}
                                                </style>
                                            </defs>
                                            <title>Билеты и услуги в одном месте</title>
                                            <path class="cls-1" d="M280.07,371.06H331.2a4,4,0,0,1,4,4v34.42a4,4,0,0,1-4,4H280.07a4,4,0,0,1-4-4V375.07a4,4,0,0,1,4-4h0Z" transform="translate(-275 -370)"/>
                                            <path class="cls-2" d="M276.06,402.95H335.2m-40.14,19.42H316.2m-18.45,0,2-8.86m13.81,8.86-2-8.86m-5.92-7.36a1.64,1.64,0,1,1-1.64,1.64,1.64,1.64,0,0,1,1.64-1.64h0Z" transform="translate(-275 -370)"/>
                                            <path class="cls-3" d="M284.76,392.4H295.4V398H284.76V392.4h0Zm16.08,0h10.64V398H300.84V392.4h0Zm15.15,0h10.64V398H316V392.4h0Z" transform="translate(-275 -370)"/>
                                            <polygon class="cls-4" points="9.64 10.59 51.63 10.59 51.63 19.21 9.64 19.21 9.64 10.59 9.64 10.59"/>
                                            <line class="cls-3" x1="10.05" y1="6.9" x2="22.27" y2="6.9"/>
                                            <path class="cls-5" d="M324.52,376.9h2.1m-7.13,0h2.1m-7.13,0h2.1" transform="translate(-275 -370)"/>
                                        </svg>
                                    </div>
                                    <div class="advantage__item_title">Билеты и услуги в одном месте</div>
                                    <div class="advantage__item_text">Не нужно открывать разные вкладки, сравнивать цены и бронировать услуги на разных сайтах.</div>
                                </div>
                            </div>
                            <div class="advantage__col">
                                <div class="advantage__item advantage__item_02">
                                    <div class="advantage__item_icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 49.8 53.15" class="ico-svg">
                                            <defs>
                                                <style>
                                                    .cls-1{fill:none; stroke-linecap:round;stroke-linejoin:round;stroke-width:2.13px;}
                                                </style>
                                            </defs><title>Все данные под рукой</title>
                                            <path class="cls-1" d="M287.63,402.11h12.19v11H287.63v-11h0Zm23.2,14.44h17.91v5.54H310.83v-5.54h0Zm16.34,0v-3.22l1.24-3.68v-4.93c0-2.32-1.4-3.69-3.93-4.28l-6.77-1.19v-7.46c0-4-4.73-4.23-4.73,0v11.59l-3.09-2.29c-2.24-1.34-4.88.35-2.39,3.58,1.91,3,2.87,6.83,5.72,8.91,0,1-1,3,0,3h13.93Zm-35.79-27.22,3.18,3,6.37-7.2m-0.76,6.68v3.7H287.81v-11h7.87m27,0.88H305.88m3.62,4.66h-3.62m3.62,4.44h-3.62m-21.28-20h1.78m2.4,0h1.78m16.64,45.39H281.06V371.06h46.38v25.46m0-18.64H281.06" transform="translate(-280 -370)"/>
                                        </svg>
                                    </div>
                                    <div class="advantage__item_title">Все данные под рукой</div>
                                    <div class="advantage__item_text">Документы и данные обо всех командировках надежно хранятся в личном кабинете.</div>
                                </div>
                            </div>
                            <div class="advantage__col">
                                <div class="advantage__item advantage__item_03">
                                    <div class="advantage__item_icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 59.12 53.29" class="ico-svg">
                                            <defs>
                                                <style>
                                                    .cls-1{fill:none; stroke-linecap:round;stroke-linejoin:round;stroke-width:2.13px;}
                                                </style>
                                            </defs>
                                            <title>Экономия времени</title>
                                            <path class="cls-1" d="M321.86,397.84a12.2,12.2,0,1,1-12.2,12.2,12.2,12.2,0,0,1,12.2-12.2h0Zm7.33,2.46,1.84-2.44m-16.87,2.44-1.9-2.46m9.54,4.84V411m-3.63-18.69h7.27m-3.63,5.48v-5.48M332,393.61V371.06H277.06V417.2h29.1m-4.71-11.11H281.55m11.2,5h-11.2m0-27.43h19.11v16.48H281.55V383.7h0Zm-4.48-4.63H332m-50.45-3.84h1m2.64,0h1" transform="translate(-276 -370)"/>
                                        </svg>
                                    </div>
                                    <div class="advantage__item_title">Экономия времени</div>
                                    <div class="advantage__item_text">Бронирование билетов и отелей занимает не более 5 минут. После поездки поможем составить командировочный отчет.</div>
                                </div>
                            </div>
                            <div class="advantage__col">
                                <div class="advantage__item advantage__item_04">
                                    <div class="advantage__item_icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.21 54.66" class="ico-svg">
                                            <defs>
                                                <style>
                                                    .cls-1{fill:none; stroke-miterlimit:22.93;stroke-width:2.13px;}
                                                </style>
                                            </defs>
                                            <title>Отчетность</title>
                                            <path class="cls-1" d="M309.94,413.46h-27.5a2.49,2.49,0,0,1-2.38-2.38V372.76c0-1.65.63-2.7,2.7-2.7h45.57a2.53,2.53,0,0,1,2.81,2.81V411c0,1.63-.69,2.3-1.56,2.3h-4.51M284.7,381.7h16.72v11.53H284.7V381.7h0ZM284,399h17.44M284,404h17.44M284,408.7h12.13M305.8,383h20.59M305.8,387.6h14.32M282.55,377h46.1m-44.77-3.41h1.92m1.34,0h1.92m31.85,38.11v10l-3.33-1.92-3.67,2.12V411.54l0.59,0.69,3-.76,3,0.71,0.44-.53h0Zm-0.59-18.14,2,2.34,2.86,1.14,0.25,3.07,1.64,2.6-1.6,2.63-0.2,3.07-2.84,1.18-2,2.37-3-.71-3,.76-2-2.34-2.86-1.14-0.25-3.07-1.64-2.6,1.6-2.63,0.2-3.07,2.84-1.18,2-2.37,3,0.71,3-.76h0Zm-2.91,5.63a3.59,3.59,0,1,1-3.59,3.59,3.59,3.59,0,0,1,3.59-3.59h0Z" transform="translate(-279 -369)"/>
                                        </svg>
                                    </div>
                                    <div class="advantage__item_title">Отчетность</div>
                                    <div class="advantage__item_text">Закрывающие документы в электронном виде. Подпишем их с ЭЦП и отправим в 1С или личный кабинет оператора ЭДО.</div>
                                </div>
                            </div>
                            <div class="advantage__col">
                                <div class="advantage__item advantage__item_05">
                                    <div class="advantage__item_icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.04 53.05" class="ico-svg">
                                            <defs>
                                                <style>
                                                    .cls-1{fill:none; stroke-linecap:round;stroke-linejoin:round;stroke-width:2.13px;}
                                                </style>
                                            </defs>
                                            <title>Понятные и прозрачные цены</title>
                                            <path class="cls-1" d="M331,371.07H284.61c-5.42-.15-6.06,7.54,0,8H330.7v43H280.06V376.09c0-3.11,1.52-5,4.55-5m0,8c-2.28,0-3.68-1.45-4.51-3.83M310,391.45H330.7v18.21H310a1.24,1.24,0,0,1-1.23-1.23V392.68a1.24,1.24,0,0,1,1.23-1.23h0Zm6.5,5.74a3.37,3.37,0,1,1-3.37,3.37,3.37,3.37,0,0,1,3.37-3.37h0Zm-30.39-22.05H331" transform="translate(-279 -370)"/>
                                        </svg>
                                    </div>
                                    <div class="advantage__item_title">Понятные и прозрачные цены</div>
                                    <div class="advantage__item_text">Стоимость на билеты и отели без скрытых комиссий и сборов. С помощью нашей умной аналитики мы поможем правильно настроить travel-политику для еще большей экономии. </div>
                                </div>
                            </div>
                            <div class="advantage__col">
                                <div class="advantage__item advantage__item_06">
                                    <div class="advantage__item_icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60.74 52.97" class="ico-svg">
                                            <defs>
                                                <style>
                                                    .cls-1{fill:none; stroke-linecap:round;stroke-linejoin:round;stroke-width:2.13px;}
                                                </style>
                                            </defs>
                                            <title>Подробная статистика</title>
                                            <path class="cls-1" d="M280,371.06h50.67a4,4,0,0,1,4,4v34.12a4,4,0,0,1-4,4H280a4,4,0,0,1-4-4V375a4,4,0,0,1,4-4h0Zm-4,31.6h58.62M294.9,421.91h20.94m-18.28,0,1.94-8.79m13.68,8.79-1.94-8.79m-5.87-7.29a1.63,1.63,0,1,1-1.63,1.63,1.63,1.63,0,0,1,1.63-1.63h0Zm-12.11-29.28a10.12,10.12,0,1,1-10.12,10.12,10.12,10.12,0,0,1,10.12-10.12h0Zm-1.67.14,1.67,10,7.61,6.66m-7.43-6.5,7.25-7m8-1.09h18.76m-18.76,5.39h18.76m-18.76,5.34h18.76m-18.76,5.44H320.1" transform="translate(-275 -370)"/>
                                        </svg>
                                    </div>
                                    <div class="advantage__item_title">Подробная статистика</div>
                                    <div class="advantage__item_text">Командировочные отчеты и расходные документы по каждому сотруднику, включающие дополнительные траты.</div>
                                </div>
                            </div>
                            <div class="advantage__col">
                                <div class="advantage__item advantage__item_07">
                                    <div class="advantage__item_icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56.32 53.1" class="ico-svg">
                                            <defs>
                                                <style>
                                                    .cls-1,.cls-3{fill:none; stroke-width:2.13px;}
                                                    .cls-1{stroke-linecap:round;stroke-linejoin:round;}
                                                    .cls-3{stroke-miterlimit:22.93;}
                                                </style>
                                            </defs>
                                            <title>Удобно оплачивать</title>
                                            <path class="cls-1" d="M322,398.8a4.69,4.69,0,1,1-4.69,4.69A4.69,4.69,0,0,1,322,398.8h0Zm-31.5-5.21h40a1.8,1.8,0,0,1,1.79,1.79v24.86a1.8,1.8,0,0,1-1.79,1.79h-40a1.8,1.8,0,0,1-1.79-1.79V395.39a1.8,1.8,0,0,1,1.79-1.79h0Zm3.15,20.34h4.15m5.52,0h4.15m5.59,0h4.15m5.37,0h4.15" transform="translate(-277 -370)"/><path class="cls-2" d="M279.8,370h40a2.8,2.8,0,0,1,2,.82h0a2.79,2.79,0,0,1,.82,2v16.14h-2V372.8a0.77,0.77,0,0,0-.23-0.55h0a0.78,0.78,0,0,0-.55-0.23h-40a0.78,0.78,0,0,0-.55.23h0a0.77,0.77,0,0,0-.23.55v24.86a0.77,0.77,0,0,0,.23.55h0a0.77,0.77,0,0,0,.55.23H285v2h-5.2a2.8,2.8,0,0,1-2-.82h0a2.79,2.79,0,0,1-.82-2V372.8a2.8,2.8,0,0,1,.82-2h0a2.8,2.8,0,0,1,2-.82h0Z" transform="translate(-277 -370)"/>
                                            <path class="cls-3" d="M278,378h43.55M278,384.63h43.55" transform="translate(-277 -370)"/>
                                        </svg>
                                    </div>
                                    <div class="advantage__item_title">Удобно оплачивать</div>
                                    <div class="advantage__item_text">Оплата билетов и отелей напрямую с расчетного счета. Не нужно пополнять депозиты и выдавать сотрудникам     наличные.</div>
                                </div>
                            </div>
                            <div class="advantage__col">
                                <div class="advantage__item advantage__item_08">
                                    <div class="advantage__item_icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 61.76 54.16" class="ico-svg">
                                            <defs>
                                                <style>
                                                    .cls-1{}
                                                    .cls-2,.cls-3{fill:none; stroke-width:2.13px;}
                                                    .cls-2{stroke-miterlimit:22.93;}
                                                    .cls-3{stroke-linecap:round;stroke-linejoin:round;}
                                                </style>
                                            </defs>
                                            <title>Один договор</title>
                                            <path class="cls-1" d="M277.52,369h54.73a3.53,3.53,0,0,1,3.52,3.52v35a3.53,3.53,0,0,1-3.52,3.52h-5a2,2,0,0,0,.73-1.24,2.49,2.49,0,0,0,.11-0.89h4.12a1.39,1.39,0,0,0,1.39-1.39v-35a1.39,1.39,0,0,0-1.39-1.39H277.52a1.39,1.39,0,0,0-1.39,1.39v35a1.39,1.39,0,0,0,1.39,1.39h35a2.56,2.56,0,0,0,1.66,1.88,2.89,2.89,0,0,1,.3.24H277.52a3.53,3.53,0,0,1-3.52-3.52v-35a3.53,3.53,0,0,1,3.52-3.52h0Z" transform="translate(-274 -369)"/>
                                            <path class="cls-2" d="M322.53,394.5l2,2.39L327.5,398l0.25,3.14,1.68,2.66-1.64,2.69-0.2,3.14-2.9,1.21-2,2.42-3.06-.73-3,.78-2-2.39-2.92-1.16-0.25-3.14L309.67,404l1.64-2.69,0.2-3.14,2.9-1.21,2-2.42,3.06,0.73,3-.78h0Zm-3,5.75a3.67,3.67,0,1,1-3.67,3.67,3.67,3.67,0,0,1,3.67-3.67h0Z" transform="translate(-274 -369)"/>
                                            <path class="cls-3" d="M320.46,418.64l1.47,3.46,1.74-4.19,4,1.62-2.6-8.35m-5.2,1.83-3.09,8.87-2.08-4.46-3.94,1.83,1.71-5.66m-13.84-38.34h12.54m-19.57,3.39h26.6m-36.77,5.42h46.93m-46.93,4.74h46.93m-46.93,4.83h23m-12.48,6.23c3.62-1.54,5.51,1.62,4.64,3.94-0.75,2-2.55,2.18-4.31,1.4a3.1,3.1,0,0,1-1.87-2.89c0.21-2.94-2.92-2.51-2.78,0-0.51,1.09-1.11,1.4-1.9.05-0.54-1-1.08-1-1.62.23-0.91.87-1.44,0.78-1.67-.1a0.92,0.92,0,0,0-1.09-1.06" transform="translate(-274 -369)"/>
                                        </svg>
                                    </div>
                                    <div class="advantage__item_title">Один договор</div>
                                    <div class="advantage__item_text">Все закрывающие документы от единого поставщика.</div>
                                </div>
                            </div>
                            <div class="advantage__col">
                                <div class="advantage__item advantage__item_09">
                                    <div class="advantage__item_icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66.88 47.75" class="ico-svg">
                                            <defs>
                                                <style>
                                                    .cls-1{}
                                                    .cls-2,.cls-3{fill:none; stroke-width:2.13px;}
                                                    .cls-2{stroke-miterlimit:22.93;}
                                                    .cls-3{stroke-linecap:round;stroke-linejoin:round;}
                                                </style>
                                            </defs>
                                            <title>Специальные предложения</title>
                                            <path class="cls-1" d="M275.34,374.77h22.23a2,2,0,0,0,.31,2.13H275.34a1.22,1.22,0,0,0-1.22,1.22V417.4a1.22,1.22,0,0,0,1.22,1.22h57.28a1.22,1.22,0,0,0,1.22-1.22v-4.46a2.84,2.84,0,0,0,2.13.69v3.77a3.36,3.36,0,0,1-3.34,3.34H275.34A3.36,3.36,0,0,1,272,417.4V378.12a3.36,3.36,0,0,1,3.34-3.34h0Zm32.75,0h24.52a3.36,3.36,0,0,1,3.34,3.34v24.45a2.82,2.82,0,0,0-2.13-.71V378.12a1.22,1.22,0,0,0-1.22-1.22H310.39a4.72,4.72,0,0,0-.74-1.06,4,4,0,0,0-1.26-.85l-0.29-.22h0Z" transform="translate(-272 -373)"/>
                                            <path class="cls-1" d="M295.62,373h11.91a1.06,1.06,0,0,1,.81.38l7.64,7.74-1.08-.26-1,1.18-6.83-6.92h-8.93l11.28,11.42-0.11,1.68-0.46.76-14-14.18a1.06,1.06,0,0,1,.76-1.8h0Zm32.18,20.07L338.57,404a1.06,1.06,0,0,1,.3.81l-0.25,11.74a1.06,1.06,0,0,1-1.81.72l-16-16.2,0.92,0.22,1-1.25,13.8,14,0.19-8.83-9.56-9.68,0.1-1.57,0.51-.83h0Z" transform="translate(-272 -373)"/>
                                            <polygon class="cls-2" points="49.56 7.8 51.79 10.4 54.97 11.67 55.24 15.09 57.07 17.98 55.29 20.91 55.07 24.32 51.91 25.64 49.72 28.27 46.39 27.48 43.07 28.33 40.84 25.73 37.66 24.46 37.38 21.05 35.55 18.15 37.34 15.23 37.56 11.81 40.72 10.49 42.91 7.86 46.24 8.65 49.56 7.8 49.56 7.8"/>
                                            <path class="cls-3" d="M284.28,389.4a4.51,4.51,0,1,1-4.25,4.5,4.38,4.38,0,0,1,4.25-4.5h0Zm12.86,13a4.51,4.51,0,1,1-4.25,4.5,4.38,4.38,0,0,1,4.25-4.5h0Zm-15.48,8.93,17.87-22" transform="translate(-272 -373)"/>
                                        </svg>
                                    </div>
                                    <div class="advantage__item_title">Специальные предложения</div>
                                    <div class="advantage__item_text">У нас заключены прямые контракты с авиакомпаниями и отелями, поэтому мы предлагаем самые низкие цены.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="benefit">
                <div class="container">
                    <h2 class="text-center">
                        Если использовать сервис<br/>
                        Бизнестур.Онлайн
                    </h2>
                    <div class="benefit__row">
                        <div class="benefit__item">
                            <div class="benefit__icon">
                                <img src="img/benefit__80.png" class="img-fluid" alt="">
                                <span>80<sub>%</sub></span>
                            </div>
                            <div class="benefit__chart">
                                <div class="benefit__chart_item" id="benefit__chart_01"></div>
                            </div>
                            <div class="benefit__title">Времени</div>
                            <div class="benefit__text">вы экономите на организацию командировок</div>
                        </div>
                        <div class="benefit__item">
                            <div class="benefit__icon">
                                <img src="img/benefit__80.png" class="img-fluid" alt="">
                                <span>20<sub>%</sub></span>
                            </div>
                            <div class="benefit__chart">
                                <div class="benefit__chart_item" id="benefit__chart_02"></div>
                            </div>
                            <div class="benefit__title">Расходов</div>
                            <div class="benefit__text">вы экономите за счет единого поставщика</div>
                        </div>
                        <div class="benefit__item">
                            <div class="benefit__icon">
                                <img src="img/benefit__80.png" class="img-fluid" alt="">
                                <span>100<sub>%</sub></span>
                            </div>
                            <div class="benefit__chart">
                                <div class="benefit__chart_item" id="benefit__chart_03"></div>
                            </div>
                            <div class="benefit__title">Поездок</div>
                            <div class="benefit__text">будут прозрачными и соответствовать политике компании</div>
                        </div>
                    </div>
                    <div class="text-center">
                        <a href="#" class="btn btn_shadow">Зарегистрироваться</a>
                    </div>
                </div>
            </section>

            <section class="partners" id="partnres">
                <div class="container">
                    <h2 class="text-center">Партнеры</h2>
                    <div class="subtitle">Мы ведём сотрудничество с крупнейшими компаниями России и Мира</div>
                    <ul class="partners__info">
                        <li>
                            <div class="partners__info_wrap">
                                <strong>250+</strong>
                                <span>Более 250 авиакомпаний, для построения самых удобных и быстрых маршрутов</span>
                            </div>
                        </li>
                        <li>
                            <div class="partners__info_wrap">
                                <strong>1 млн+</strong>
                                <span>Более 1 млн. гостиниц, отелей, апартаментов и хостелов по всему Миру</span>
                            </div>
                        </li>
                    </ul>
                    <div class="partners_slider swiper-container">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
                            <div class="swiper-slide">
                                <div class="partners__logo">
                                    <i><img src="img/partners/p_logo__01.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="partners__logo">
                                    <i><img src="img/partners/p_logo__02.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="partners__logo">
                                    <i><img src="img/partners/p_logo__03.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="partners__logo">
                                    <i><img src="img/partners/p_logo__04.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="partners__logo">
                                    <i><img src="img/partners/p_logo__05.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="partners__logo">
                                    <i><img src="img/partners/p_logo__06.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="partners__logo">
                                    <i><img src="img/partners/p_logo__07.png" class="img-fluid" alt=""></i>
                                </div>
                            </div>
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </section>

            <section class="service">
                <div class="container">
                    <div class="service__row">
                        <div class="service__content">
                            <h2>Круглосуточный сервис поддержки</h2>
                            <div class="service__text">
                                <p>Вы всегда можете обратиться к нам для изменения бронирования или отмены. Поздний вылет, выезд, или вас необходимо разбудить – сообщите нам и мы позаботимся об этом. Мы на связи 24/7.</p>
                                <p>Звоните по телефону <a href="tel:+7(999)1234567">+7 (999) 123 45 67</a> или пишите в чат.</p>
                            </div>
                        </div>
                        <div class="service__image">
                            <div class="service__image_wrap">
                                <img src="img/service__bg.png" class="img-fluid" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="faq" id="faq">
                <div class="container">
                    <h2 class="text-center">Часто задаваемые вопросы</h2>
                    <div class="subtitle">Если у Вас еще остались вопросы, Вы можете воспользоваться  этим разделом</div>


                    <div class="faq__item open">
                        <div class="faq__item_question">Бизнестур.онлайн - сайт-агрегатор билетов и отелей?</div>
                        <div class="faq__item_answer" style="display: block">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nulla magna, vulputate sit amet suscipit id, pellentesque a risus. Aenean pellentesque, enim in lacinia faucibus, odio turpis dapibus magna, in tincidunt velit nibh in justo. Donec lacinia orci lectus, eu sagittis odio finibus et. Nulla facilisi. Nullam facilisis vel urna a scelerisque. </p>
                        </div>
                    </div>

                    <div class="faq__item">
                        <div class="faq__item_question">Возможно ли предоставить сотрудникам доступ к сервису, чтобы они самостоятельно могли заниматься бронированием и покупкой?</div>
                        <div class="faq__item_answer">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nulla magna, vulputate sit amet suscipit id, pellentesque a risus. Aenean pellentesque, enim in lacinia faucibus, odio turpis dapibus magna, in tincidunt velit nibh in justo. Donec lacinia orci lectus, eu sagittis odio finibus et. Nulla facilisi. Nullam facilisis vel urna a scelerisque. </p>
                        </div>
                    </div>

                    <div class="faq__item">
                        <div class="faq__item_question">На чем зарабатывает бизнестур.онлайн?</div>
                        <div class="faq__item_answer">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nulla magna, vulputate sit amet suscipit id, pellentesque a risus. Aenean pellentesque, enim in lacinia faucibus, odio turpis dapibus magna, in tincidunt velit nibh in justo. Donec lacinia orci lectus, eu sagittis odio finibus et. Nulla facilisi. Nullam facilisis vel urna a scelerisque. </p>
                        </div>
                    </div>

                    <div class="faq__item">
                        <div class="faq__item_question">Как происходит обмен закрывающими (бухгалтерскими) документами?</div>
                        <div class="faq__item_answer">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nulla magna, vulputate sit amet suscipit id, pellentesque a risus. Aenean pellentesque, enim in lacinia faucibus, odio turpis dapibus magna, in tincidunt velit nibh in justo. Donec lacinia orci lectus, eu sagittis odio finibus et. Nulla facilisi. Nullam facilisis vel urna a scelerisque. </p>
                        </div>
                    </div>

                    <div class="faq__item">
                        <div class="faq__item_question">Какие предусмотрены способы оплаты?</div>
                        <div class="faq__item_answer">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nulla magna, vulputate sit amet suscipit id, pellentesque a risus. Aenean pellentesque, enim in lacinia faucibus, odio turpis dapibus magna, in tincidunt velit nibh in justo. Donec lacinia orci lectus, eu sagittis odio finibus et. Nulla facilisi. Nullam facilisis vel urna a scelerisque. </p>
                        </div>
                    </div>

                    <div class="faq__item">
                        <div class="faq__item_question">Какие travel-услуги можно купить в бизнестур.онлайн?</div>
                        <div class="faq__item_answer">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nulla magna, vulputate sit amet suscipit id, pellentesque a risus. Aenean pellentesque, enim in lacinia faucibus, odio turpis dapibus magna, in tincidunt velit nibh in justo. Donec lacinia orci lectus, eu sagittis odio finibus et. Nulla facilisi. Nullam facilisis vel urna a scelerisque. </p>
                        </div>
                    </div>

                </div>

            </section>

            <footer class="footer">
                <div class="container">
                    <div class="footer__row">
                        <div class="footer__right">
                            <nav class="footer__nav scroll_nav">
                                <div class="footer__title">Навигация</div>
                                <ul>
                                    <li><a href="#service">О сервисе</a></li>
                                    <li><a href="#advantage">Преимущества</a></li>
                                    <li><a href="#partnres">Партнеры</a></li>
                                    <li><a href="#faq">Часто задаваемые вопросы</a></li>
                                </ul>
                            </nav>
                            <div class="footer__contact">
                                <div class="footer__title">Контакты</div>
                                <ul>
                                    <li>Отдел продаж: <a href="tel:+79991234567">+7 (999) 123-45-67</a></li>
                                    <li>Техподдержка: <a href="tel:+79991234567">+7 (999) 123-45-67</a></li>
                                    <li>info@businesstour.ru</li>
                                    <li>Москва, Южнопортовая, 5с1</li>
                                </ul>
                            </div>
                            <div class="footer__social hide">
                                <div class="footer__title">Соц.сети</div>
                                <div class="footer__social_row">
                                    <a href="#">
                                        <div>
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 90 90" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__youtube" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div>
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 169.063 169.063" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__instagram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div>
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 612 612" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__twitter" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div>
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 95.481 95.481" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__ok" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div>
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 548.358 548.358" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__vk" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </a>
                                    <a href="#">
                                        <div>
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 96.124 96.123" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__fb" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="footer__left">
                            <div class="footer__logo">
                                <div class="footer__logo_wrap">
                                    <a href="#">
                                        <img src="img/logo.svg" class="img-fluid" alt="">
                                    </a>
                                    <div class="footer__info">
                                        ООО «БИЗНЕСТУР.ОНЛАЙН»<br/>
                                        ИНН 1655393869 ОГРН 1171690109414<br/>
                                        420107, Республика Татарстан,<br/>
                                        г.Казань, ул.Петербургская, дом 25В
                                    </div>
                                </div>
                            </div>
                            <div class="footer__text"><span>© Бизнестур.онлайн 2019</span></div>
                        </div>
                    </div>
                </div>
            </footer>

        </div>


        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
        <script src="js/vendor/svg4everybody.legacy.min.js"></script>
        <script src="js/vendor/circles.min.js"></script>
        <script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
