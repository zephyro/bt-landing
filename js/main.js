$( document ).ready(function() {

    (function() {

        var TxtRotate = function(el, toRotate, period) {
            this.toRotate = toRotate;
            this.el = el;
            this.loopNum = 0;
            this.period = parseInt(period, 10) || 1000;
            this.txt = '';
            this.tick();
            this.isDeleting = false;
        };

        TxtRotate.prototype.tick = function() {
            var i = this.loopNum % this.toRotate.length;
            var fullTxt = this.toRotate[i];

            if (this.isDeleting) {
                this.txt = fullTxt.substring(0, this.txt.length - 1);
            } else {
                this.txt = fullTxt.substring(0, this.txt.length + 1);
            }

            this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

            var that = this;
            var delta = 125 - Math.random() * 100;

            if (this.isDeleting) { delta /= 2; }

            if (!this.isDeleting && this.txt === fullTxt) {
                delta = this.period;
                this.isDeleting = true;
            } else if (this.isDeleting && this.txt === '') {
                this.isDeleting = false;
                this.loopNum++;
                delta = 500;
            }

            setTimeout(function() {
                that.tick();
            }, delta);
        };

        var elements = document.getElementsByClassName('txt-rotate');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-rotate');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
                new TxtRotate(elements[i], JSON.parse(toRotate), period);
            }
        }

    }());

    (function() {

        $('.scroll_nav a').on('click touchstart', function(){
            var ww = $(window).width();
            var lnk = $(this).attr('href');
            $('.header').removeClass('nav_open');
            if (ww > 1199) {
                $('html, body').animate({
                    scrollTop: $(lnk).offset().top
                }, 600);
            }
            else {
                $('html, body').animate({
                    scrollTop: $(lnk).offset().top - 70
                }, 600);
            }
        });

    }());

// SVG IE11 support
    svg4everybody();


// Nav
    (function() {

        $('.nav_toggle').on('click touchstart', function(e){
            e.preventDefault();
            $('.header').toggleClass('nav_open');
        });

    }());


    var vendor = new Swiper ('.vendor_slider', {
        loop: false,
        spaceBetween: 10,
        slidesPerView: 2,
        slidesPerGroup: 1,
        breakpoints: {
            1310: {
                slidesPerView: 7,
                spaceBetween: 10,
            },
            1200: {
                slidesPerView: 6,
                spaceBetween: 10,
            },
            992: {
                slidesPerView: 4,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 10,
            },
            480: {
                slidesPerView: 2,
                spaceBetween: 10,
            }
        }
    });


    var advantage = new Swiper ('.advantage_slider', {
        loop: false,
        spaceBetween: 30,
        slidesPerView: 1,
        slidesPerGroup: 1,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        breakpoints: {
            768: {
                slidesPerView: 2,
                slidesPerGroup: 2
            }
        }
    });

    var partners = new Swiper ('.partners_slider', {
        loop: false,
        spaceBetween: 14,
        slidesPerView: 2,
        slidesPerGroup: 1,
        autoplay: true,

        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        breakpoints: {
            1310: {
                slidesPerView: 5,
                spaceBetween: 10,
            },
            1200: {
                slidesPerView: 5,
                spaceBetween: 8,
            },
            992: {
                slidesPerView: 5,
                spaceBetween: 8,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 8,
            },
            576: {
                slidesPerView: 2,
                spaceBetween: 8,
            }
        }
    });

// FAQ
    (function() {

        $('.faq__item_question').on('click touchstart', function(e){
            e.preventDefault();
            $(this).closest('.faq__item').toggleClass('open');
            $(this).closest('.faq__item').find('.faq__item_answer').slideToggle('fast');
        });

    }());



    $(".btn_modal").fancybox({
        'padding'    : 0
    });


    $(function($){
        $h = $('.benefit__row').offset().top;
        var visible = false;

        $(window).scroll(function(){
            $w =  $(window).scrollTop() + ($(window).height() / 2);

            console.log($h);
            console.log($w);

            if(!visible) {

                if ( $w > $h) {

                    visible = true;

                    var circle_01 = Circles.create({
                        id:                  'benefit__chart_01',
                        radius:              94,
                        value:               80,
                        maxValue:            100,
                        width:               6,
                        text:                function(value){return value + '%';},
                        colors:              ['#ffffff', '#1396e4'],
                        duration:            800,
                        wrpClass:            'circles-wrp',
                        textClass:           'circles-text',
                        valueStrokeClass:    'circles-valueStroke',
                        maxValueStrokeClass: 'circles-maxValueStroke',
                        styleWrapper:        true,
                        styleText:           false
                    });

                    var circle_02 = Circles.create({
                        id:                  'benefit__chart_02',
                        radius:              94,
                        value:               20,
                        maxValue:            100,
                        width:               6,
                        text:                function(value){return value + '%';},
                        colors:              ['#ffffff', '#1396e4'],
                        duration:            800,
                        wrpClass:            'circles-wrp',
                        textClass:           'circles-text',
                        valueStrokeClass:    'circles-valueStroke',
                        maxValueStrokeClass: 'circles-maxValueStroke',
                        styleWrapper:        true,
                        styleText:           false
                    });

                    var circle_03 = Circles.create({
                        id:                  'benefit__chart_03',
                        radius:              94,
                        value:               100,
                        maxValue:            100,
                        width:               6,
                        text:                function(value){return value + '%';},
                        colors:              ['#ffffff', '#1396e4'],
                        duration:            800,
                        wrpClass:            'circles-wrp',
                        textClass:           'circles-text',
                        valueStrokeClass:    'circles-valueStroke',
                        maxValueStrokeClass: 'circles-maxValueStroke',
                        styleWrapper:        true,
                        styleText:           false
                    });

                }
            }
        });
    });

});


